alot (0.10-2) UNRELEASED; urgency=medium

  * UNRELEASED
  * d/watch: Adjust pgpsigurlmangle to find signature

 -- Jordan Justen <jljusten@debian.org>  Mon, 27 Sep 2021 12:32:10 -0700

alot (0.10-1) unstable; urgency=medium

  [ Ondřej Nový ]
  * d/control: Update Maintainer field with new Debian Python Team
    contact address.
  * d/control: Update Vcs-* fields with new Debian Python Team Salsa
    layout.

  [ Jordan Justen ]
  * New upstream 0.10 release.
  * d/patches: Rebase patches for 0.10 release
  * d/control: Change python3-notmuch dependency to python3-notmuch2
  * d/control: Bump python3-twisted dep to 18.4.0 from upstream
  * d/control: Bump python3-urwidtrees dep to 1.0.3 from upstream
  * d/rules: Skip auto test
  * d/control: Update Standards-Version to 4.6.0.1
  * d/patches: Cherry pick patch from PR 1543 until urwidtrees 1.0.3 is
    in debian

 -- Jordan Justen <jljusten@debian.org>  Mon, 27 Sep 2021 10:32:01 -0700

alot (0.9.1-2) unstable; urgency=medium

  * Backport two commits from upstream
     - Fix utf8 encoding with a text/plain mailcap entry (closes: #964391)
     - Fix mailcap rendering for e-mails without `Content-Type` header
       (closes: #964501)

 -- Johannes 'josch' Schauer <josch@debian.org>  Fri, 10 Jul 2020 14:06:29 +0200

alot (0.9.1-1) unstable; urgency=medium

  [ Jordan Justen ]
  * New upstream 0.9.1 release. Closes: #955193
  * d/control: Update Jordan's email address
  * d/patches: Rebase patches for 0.9.1
  * d/control: Update to debhelper 13
  * d/control: Remove python3-mock dep, as it's now part of py stdlib
  * d/control: Bump python dependency to python 3.6
  * d/control: Require python3-notmuch 0.27 or newer
  * d/control: Bump twisted dependency to 18.4.0

  [ Debian Janitor ]
  * Set upstream metadata fields: Bug-Database, Bug-Submit.

 -- Jordan Justen <jljusten@debian.org>  Sun, 05 Jul 2020 01:19:27 -0700

alot (0.9-2) unstable; urgency=medium

  * d/patches: Add upstream patch 28a4296, Closes: #951504
  * d/control: Depend on python3-gpg (>= 1.13.1-6). Closes: #951308

 -- Jordan Justen <jordan.l.justen@intel.com>  Thu, 12 Mar 2020 14:43:44 -0700

alot (0.9-1) unstable; urgency=medium

  [ Jordan Justen ]
  * New upstream 0.9 release
  * d/patches: Rebase patches for 0.9 release
  * d/control: Update Standards-Version to 4.5.0

  [ Ondřej Nový ]
  * Bump Standards-Version to 4.4.1.

  [ Debian Janitor ]
  * Bump debhelper from old 11 to 12.
  * Re-export upstream signing key without extra signatures.
  * Set upstream metadata fields: Repository, Repository-Browse.

 -- Jordan Justen <jordan.l.justen@intel.com>  Tue, 11 Feb 2020 13:51:59 -0800

alot (0.8.1-2) unstable; urgency=medium

  [ Ondřej Nový ]
  * Use debhelper-compat instead of debian/compat.
  * Bump Standards-Version to 4.4.0.

  [ Jordan Justen ]
  * d/patches: Cherry pick upstream 3446dc44f87d. Closes: #930057

 -- Jordan Justen <jordan.l.justen@intel.com>  Tue, 06 Aug 2019 15:34:16 -0700

alot (0.8.1-1) unstable; urgency=medium

  * New upstream 0.8.1 release
  * d/watch: Fix watch file format for patch versions
  * d/patches: Rebase patches for v0.8.1
  * d/control: Update Standards-Version to 4.3.0

 -- Jordan Justen <jordan.l.justen@intel.com>  Sat, 23 Feb 2019 00:17:34 -0800

alot (0.8-2) unstable; urgency=medium

  * Add patch that fixes a race condition in tests (closes: #906335)

 -- Johannes 'josch' Schauer <josch@debian.org>  Tue, 12 Feb 2019 06:52:47 +0100

alot (0.8-1) unstable; urgency=medium

  [ Jordan Justen ]
  * New upstream 0.8 release. Closes: #916277
  * d/patches: Rebase patches for 0.8
  * d/watch: Split parameters onto separate lines
  * d/watch: Adjust pgp filename for 0.8 release
  * d/control: 0.8 now uses python3
  * d/rules: 0.8 now uses python3
  * d/patches: Use gpg rather than gpg2
  * d/patches: Patch test_env_set to call python3
  * d/control: Update Standards-Version to 4.2.1
  * d/lintian-overrides: Remove build-depends-on-python-sphinx-only override
  * d/lintian-overrides: Remove testsuite-autopkgtest-missing override
  * d/patches: Add patches to fix upstream issue #1360 (dropped chars)

  [ Johannes 'josch' Schauer ]
  * d/control: Add myself to Uploaders

 -- Johannes 'josch' Schauer <josch@debian.org>  Fri, 08 Feb 2019 23:09:20 +0100

alot (0.7-2) unstable; urgency=medium

  * d/alot-doc: Remove symlink from alot-doc package
  * d/control: Update Standards-Version to 4.2.0
  * d/patches: Update patches for lintian quilt-patch-missing-description
  * d/source: Override lintian build-depends-on-python-sphinx-only
  * d/scripts: Use symlink_to_dir to fix piuparts upgrade failure
  * d/watch: Update github url to check the releases url
  * d/patches: Apply patch for unicode char search bug. Closes: #880172
  * d/scripts: Use single alot-doc.maintscript script
  * d/alot-doc.maintscript: Fix symlink_to_dir package version. Closes: #905193

 -- Jordan Justen <jordan.l.justen@intel.com>  Fri, 17 Aug 2018 12:59:45 -0700

alot (0.7-1) unstable; urgency=low

  [Ondřej Nový]
  * d/changelog: Remove trailing whitespaces
  * d/control: Remove ancient X-Python-Version field

  [Jordan Justen]
  * New upstream 0.7 release
  * d/watch: Remove "v" from tarball
  * Remove unused-override vcs-field-uses-insecure-uri for litian warning
  * Update to debhelper 11
  * Update Standards-Version to 4.1.4

 -- Jordan Justen <jordan.l.justen@intel.com>  Fri, 08 Jun 2018 21:21:59 -0700

alot (0.6-2.1) unstable; urgency=high

  * Non-maintainer upload
  * Drop "Use file-magic instead of python-magic" patch. Closes: #889293

 -- Christoph Biedl <debian.axhn@manchmal.in-ulm.de>  Sun, 04 Feb 2018 18:24:59 +0100

alot (0.6-2) unstable; urgency=low

  * Depend on python-gpg rather than pygpgme for alot package
    (Closes: #846314, #866027)

 -- Jordan Justen <jordan.l.justen@intel.com>  Wed, 06 Sep 2017 17:41:22 -0700

alot (0.6-1) unstable; urgency=low

  * New upstream release
  * Add PGP signature validation to watch file with pazz's signature
    with fingerprint B7838D65FEE80DED1DCA494D94340367D7D6C5AA.
  * Depend on python-gpg rather than pygpgme (Closes: #846314, #866027)
  * Update debian patches for 0.6 release
  * Add procps build-dependency since ps is used by tests
  * Set LC_ALL=C.UTF-8 in rules to fix two failing tests that make use of
    unicode characters.

 -- Jordan Justen <jordan.l.justen@intel.com>  Sun, 20 Aug 2017 15:40:54 -0700

alot (0.5.1-2) unstable; urgency=low

  * Update Vcs-Svn url (the old one didn't work)
  * Update Standards-Version to 4.0.0 (no changes required)
  * Fix lintian debian-rules-parses-dpkg-parsechangelog by using
    SOURCE_DATE_EPOCH from /usr/share/dpkg/pkg-info.mk
  * Override lintian vcs-field-uses-insecure-uri because alioth svn
    doesn't offer a secure way to retrieve the source
  * Override lintian debian-watch-may-check-gpg-signature until upstream
    implements it
  * Override lintian testsuite-autopkgtest-missing as a TODO item
  * Release to unstable since last uploads were made to experimental
    during the Stretch freeze

 -- Jordan Justen <jordan.l.justen@intel.com>  Mon, 10 Jul 2017 09:52:43 -0700

alot (0.5.1-1) experimental; urgency=low

  [ Jordan Justen ]
  * New upstream release (Closes: #848150, #792108).
  * Add new patch for python-magic=>file-magic rename
  * Add Jordan as uploader.
  * Update source compat from 9 to 10
  * Use debhelper pybuild (Closes: #855559)
  * Adjust dependencies based on setup.py and removed dependencies
    where oldstable/wheezy already met the requirement

  [ Hugo Lefeuvre ]
  * New upstream release (Closes: #814460).
  * Run wrap-and-sort -a.
  * debian/control:
    - Add myself to the uploaders.
    - Bump Standards-Version to 3.9.8.
    - Update Homepage field.
    - Add python-setuptools and python-urwidtrees to the Build-Depends
      field (needed by new upstream release).
    - Add dh-python to the Build-Depends field.
    - Update Vcs-* fields to use encrypted protocols (https).
  * debian/copyright:
    - Update copyright years.
    - Add an entry for Hugo Lefeuvre.
    - Update Source and Format fields.
  * debian/patches:
    - Fix crash when email's date is malformated (Closes: #821459).
  * debian/rules:
    - Remove extra timestamps from the build system when creating the
      source archive (Closes: #784723).

 -- Jordan Justen <jordan.l.justen@intel.com>  Tue, 21 Feb 2017 01:14:49 -0800

alot (0.3.6-1) unstable; urgency=medium

  * New upstream release
    + Drop the GPG encoding patch, merged upstream.
  * d/copyright: update copyright years.
  * d/control: Bump Standard-Version to 3.9.5, no changes required

 -- Simon Chopin <chopin.simon@gmail.com>  Fri, 22 Aug 2014 12:58:24 +0200

alot (0.3.5-2) unstable; urgency=low

  * Drop outdated Lintian overrides for alot-doc
  * Add a patch to fix an encoding error in the GPG handling that prevented
    alot from opening some threads.

 -- Simon Chopin <chopin.simon@gmail.com>  Thu, 25 Jul 2013 11:05:43 +0200

alot (0.3.5-1) unstable; urgency=low

  [ Jakub Wilk ]
  * Use canonical URIs for Vcs-* fields.

  [ Simon Chopin ]
  * Add w3m | links to Recommends to enable HTML rendering
  * New upstream release
    + Drop the backported GPGFix patch
  * d/copyright: Bump the copyright years
  * Bump Standard-Version to 3.9.4, no change required

 -- Simon Chopin <chopin.simon@gmail.com>  Tue, 23 Jul 2013 22:51:33 +0200

alot (0.3.4-2) unstable; urgency=low

  * New patch fixing ill-instantiated GPGProblem exception, thanks to
    Vasudev Kamath (Closes: #704884)

 -- Simon Chopin <chopin.simon@gmail.com>  Tue, 09 Apr 2013 15:33:32 +0200

alot (0.3.4-1) unstable; urgency=low

  * New upstream release
  * debian/control: Add notmuch to the Recommends field
  * debian/watch: Use plain github URLs instead of githubredir

 -- Simon Chopin <chopin.simon@gmail.com>  Tue, 26 Mar 2013 21:23:58 +0100

alot (0.3.3-1) unstable; urgency=low

  * New upstream release.
  * Drop all patches, either applied upstream or became irrelevant.
  * New patch: 0007-use-local-intersphinx-links to use local debian
    documentation instead of fetching it on the Internet.

 -- Simon Chopin <chopin.simon@gmail.com>  Tue, 11 Sep 2012 21:04:24 -0400

alot (0.3.2-1) unstable; urgency=low

  * Initial release (Closes: #677522)

 -- Simon Chopin <chopin.simon@gmail.com>  Tue, 28 Aug 2012 09:47:28 -0400
